import { EnumStatus } from './enum-status';

export interface Product {
    serialNumber: string;
    location: string;
    status: EnumStatus;
}

