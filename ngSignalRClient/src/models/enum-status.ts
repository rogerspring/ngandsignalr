export enum EnumStatus {
    ordered,
    startedAssembling,
    awaitingShipping,
    shipped
}
