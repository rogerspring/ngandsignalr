import { Injectable } from '@angular/core';
import * as signalR from '@aspnet/signalr';
import { Product } from 'src/models/product.model';
import { HttpClient, HttpResponseBase } from '@angular/common/http';
import { tap } from 'rxjs/operators';

export interface SignalRProductService {
  products: Product[];
  startConnection: () => void;
  updateProduct: (product: Product) => void;
}

@Injectable({
  providedIn: 'root'
})
export class ProductService implements SignalRProductService {
  public products: Product[];
  private hubConnection: signalR.HubConnection;


  constructor(
    private readonly http: HttpClient
  ) { }
  public startConnection = () => {
    this.hubConnection = new signalR.HubConnectionBuilder()
      .withUrl('https://localhost:44388/product')
      // https://docs.microsoft.com/en-us/aspnet/core/signalr/diagnostics?view=aspnetcore-3.1
      .configureLogging(signalR.LogLevel.Debug)
      .build();

    this.hubConnection.on('broadcastproducts', (products) => {
      this.products = products;
      console.log(products);
    });

    return this.hubConnection
      .start()
      .then(() => console.log('Connection started'))
      .then(this.getCurrentProducts)
      .catch(err => console.log('Error while starting connection: ' + err));
  }

  public updateProduct = (product: Product) => {
    this.hubConnection.invoke('updateProduct', product)
      .catch(err => console.error(err));
  }

  public getCurrentProducts = () => {
    this.http.get('https://localhost:44388/api/product').pipe(
      tap((response) => {
        this.products = response as Product[];
      }),
      tap(r => console.log(r))
    ).subscribe();
  }

}
