import { Component, OnInit } from '@angular/core';

import { ProductService } from './services/signal-r.service';
import { EnumStatus } from 'src/models/enum-status';

@Component({
  selector: 'ngsr-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {

  constructor(
    public readonly productService: ProductService
  ) { }

  ngOnInit(): void {
    this.productService.startConnection();
  }

  statusAsText = (status: EnumStatus): string => {
    return EnumStatus[status];
  }

  public sendUpdate = () => {
    let prods = this.productService.products;
    if (!prods) {
      prods = [{
        location: 'Portland',
        serialNumber: '7da9f732-91f8-4468-8f1f-624aa88dc4ec',
        status: EnumStatus.ordered
      }];
    } else {
      const prod = prods[0];
      prod.status = Math.min((prods[0].status + 1) % 3, EnumStatus.shipped);
      this.productService.updateProduct(prod);
    }
  }
}
