﻿using System;
using System.Security.Cryptography.X509Certificates;
using Microsoft.EntityFrameworkCore;
using ngSignalR.Model;

namespace ngSignalR.Data
{
    public class WarehouseDBContext : DbContext
    {
        public WarehouseDBContext(DbContextOptions<WarehouseDBContext> options) : base(options)
        {}

        public DbSet<Product> Products { get; set; }
    }
}
