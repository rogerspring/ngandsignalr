﻿using ngSignalR.Data;
using System;
using System.Linq;
using Bogus;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ngSignalR.Model;

namespace ngSignalR.Service
{
    public class DataSeeder
    {
        public static void Initialize(IServiceProvider serviceProvider)
        {

            //Set the randomizer seed if you wish to generate repeatable data sets.
            Randomizer.Seed = new Random(8675309);

            using (var context = new WarehouseDBContext(
                serviceProvider.GetRequiredService<DbContextOptions<WarehouseDBContext>>()))
            {
                if (context.Products.Any())
                {
                    return;
                }
                var locations = new[] { "Portland", "Ashland", "Salem", "Brooklyn" };
                var IDs = 2;
                var seedProducts = new Faker<Product>()
                    //Ensure all properties have rules. By default, StrictMode is false
                    //Set a global policy by using Faker.DefaultStrictMode
                    .StrictMode(true)
                    //OrderId is deterministic
                    .RuleFor(p => p.ID, f => IDs++)
                    //Use an enum outside scope.
                    .RuleFor(p => p.Status, f => f.PickRandom<EnumStatus>())
                    .RuleFor(p => p.Location, f => f.PickRandom(locations))
                    .RuleFor(p => p.SerialNumber, f => Guid.NewGuid()); 


                var products = seedProducts.Generate(30).ToList();

                context.Products.AddRange(products);

                context.SaveChanges();
            }
        }
    }
}
