﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using ngSignalR.Data;

namespace ngSignalR.Service
{
    public static class CompositionRoot    {
        public static IServiceCollection AddServiceLayerDependencies(this IServiceCollection services)
        {
            services.AddDbContext<WarehouseDBContext>(
                options =>
                {
                    options.UseInMemoryDatabase(databaseName: "Warehouse");
                    options.EnableSensitiveDataLogging(true);
                }
            );

            return services;
        }

    }
}
