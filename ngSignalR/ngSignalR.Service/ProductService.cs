﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ngSignalR.Data;
using ngSignalR.Model;

namespace ngSignalR.Service
{
    public class ProductService
    {
        private WarehouseDBContext _db;

        public ProductService(WarehouseDBContext db)
        {
            _db = db;
        }

        public List<Product> GetProducts()
        {
            return _db.Products.Select(p => 
                new Product
                {
                    ID = p.ID, 
                    Location = p.Location, 
                    Status = p.Status, 
                    SerialNumber = p.SerialNumber
                }
            ).ToList();
        }

        public async Task UpdateProduct(Product product)
        {
            Product prod = _db.Products.SingleOrDefault(p => p.ID == product.ID);
            if (prod == null)
                return;

            prod.Location = product.Location;
            prod.Status = product.Status;
            await _db.SaveChangesAsync();
        }
    }
}
