﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.SignalR;
using ngSignalR.Model;
using ngSignalR.Server.HubConfig;
using ngSignalR.Service;

namespace ngSignalR.Server
{
    [Route("api/[controller]")]
    public class ProductController : Controller
    {
        private ProductService _productService;

        public ProductController(ProductService productService)
        {
            _productService = productService;
        }
        [HttpGet]
        public List<Product> GetProducts()
        {
            return _productService.GetProducts();
        }
    }
}