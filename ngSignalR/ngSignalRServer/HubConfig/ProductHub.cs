﻿using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using ngSignalR.Model;
using ngSignalR.Service;

namespace ngSignalR.Server.HubConfig
{
    public class ProductHub: Hub
    {
        private ProductService _productService;

        public ProductHub(ProductService productService)
        {
            _productService = productService;
        }

        //public async Task CurrentProducts() =>
        //    await Clients.Caller.SendAsync("currentProducts", _productService.GetProducts());

        public async Task UpdateProduct(Product product)
        {
            await _productService.UpdateProduct(product);
            var products = _productService.GetProducts();
            await Clients.All.SendAsync("broadcastproducts", products);
        }


        //public override async Task OnConnectedAsync() => await Clients.Caller.SendAsync("currentProducts", _productService.GetProducts());
    }
}
