﻿using System;

namespace ngSignalR.Model
{
    public class Product
    {
        public int ID { get; set; }
        public Guid SerialNumber { get; set; }
        public string Location { get; set; }
        public EnumStatus Status { get; set; }
    }
}
