﻿namespace ngSignalR.Model
{
    public enum EnumStatus
    {
        Unknown,
        Ordered,
        Started,
        AwaitingShipping,
        Shipped
    }
}